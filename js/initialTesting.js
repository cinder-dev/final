/*
 Caleb Snoozy
 Due Date: 3/20/2017
 Assignment: Final Project
 File: initialTesting.js
 Description: Early tests, not used.
 */

let width, height;

let canvas, context;
let bufferCanvas, bufferContext;

let history;

let functions = [
    (x, y) => {
        return {
            x: Math.abs(Math.floor(x + getRandomInt(-1, 1)) % width),
            y: Math.abs(Math.floor(y + getRandomInt(-1, 1)) % height)
        }
    },
];

window.onload = () => {
    width = window.innerWidth;
    height = window.innerHeight;

    history = new Map();
    for (let row = 0; row < width; row++) {
        history.set(row, new Map());
        for (let col = 0; col < height; col++) {
            history.get(row).set(col, 0);
        }
    }

    canvas = document.getElementById('myCanvas');
    if (canvas && canvas.getContext) {
        context = canvas.getContext('2d');

        canvas.width = width;
        canvas.height = height;

        context.fillStyle = '#000000';
        context.fillRect(-width, -height, width * 2, height * 2);

        loop();
    }
};


function loop() {
    for (let i = 0; i < 1000; i++) {
        dive(0, getRandomInt(0, functions.length - 1), getRandomInt(0, width - 1), getRandomInt(0, height - 1));
        // context.fillRect(point.x, point.y, 1, 1);
    }
    render();
}

function dive(iteration, f, x, y) {
    // console.log(`iteration: ${iteration} function: ${f} x: ${x} y: ${y}`);
    let point = functions[f](x, y);
    if (iteration > 0)
        history.get(point.x).set(point.y, history.get(point.x).get(point.y) + 1);

    if (iteration < 1000)
        dive(iteration + 1, f, point.x, point.y);
}

function render() {
    context.fillStyle = '#000000';
    context.fillRect(-width, -height, width * 2, height * 2);
    for (let row = 0; row < width; row++) {
        for (let col = 0; col < height; col++) {
            let color = history.get(row).get(col) * 15;
            if (color > 0) {
                context.fillStyle = `rgb(${color},${color*1.5},${color*2})`;
                context.fillRect(row, col, 1, 1);
            }
        }
    }

    window.addEventListener('mousemove', (event) => {
        document.title = history.get(event.clientX).get(event.clientY);
    });
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
